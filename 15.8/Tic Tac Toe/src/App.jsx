import { useState } from 'react'
import './style.scss'

function App() {
  const initialListOfBoxes = Array.from(new Array(9)).map((x, i) => ({ content: "", id: i + 1 }))
  //|
  const [listOfBoxes, setListOfBoxes] = useState(initialListOfBoxes)
  const [firstPlayerTurn, setFirstPlayerTurn] = useState(true)
  const [showGameOver, setShowGameOver] = useState(false)

  const ListOfBoxes = () => {
    return listOfBoxes.map(item => {
      return <div onClick={selectBox(item.id)} className="box" key={item.id}>{item.content}</div >
    })
  }

  const selectBox = (id) => () => {
    if (listOfBoxes[id - 1].content)
      return // ends function, prevents selection of boxes that have been selected already

    setListOfBoxes(listOfBoxes.map(item => {
      if (item.id === id) firstPlayerTurn ? item.content = "X" : item.content = "O"
      return item
    }))

    setFirstPlayerTurn(!firstPlayerTurn)

    const gameOver = !listOfBoxes.find(x => !x.content)
    if (gameOver) setShowGameOver(true)
  }

  const resetGame = () => {
    setFirstPlayerTurn(true)
    setShowGameOver(false)
    setListOfBoxes(initialListOfBoxes)
  }


  return (
    <section className="App">
      Hello to Tic Tac Toe

      <div className="board">
        <ListOfBoxes />
      </div>

      <div className="resetButtonContainer">
        <div onClick={resetGame} className="resetButton"  >Reset </div>
        {showGameOver && <span className="gameOverText">Game Over</span>}
      </div>

    </section>
  )
}

export default App
