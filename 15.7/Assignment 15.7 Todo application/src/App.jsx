import { useState } from 'react'
import './style.scss'
import SvgComponent from './components/SvgComponent'

function App() {

  const [taskList, setTaskList] = useState([])
  const [taskInputValue, setTaskInputValue] = useState("")
  const [idAcc, setIdAcc] = useState(1)


  const onTaskInput = (event) => {
    setTaskInputValue(event.target.value)
  }

  const submitTask = () => {
    if (!taskInputValue.length)
      return // wont allow creation of empty tasks

    setTaskList(taskList.concat([{ id: idAcc, task: taskInputValue, done: false }]))
    setIdAcc(idAcc + 1);
    setTaskInputValue("")
  }

  const removeTask = (id) => () => {
    setTaskList(taskList.filter(item => item.id != id))
  }

  const toggleTask = (id) => () => {
    setTaskList(taskList.map(item => {
      if (item.id === id) item.done = !item.done
      return item
    }))
  }

  const ListOfTasks = () => {
    const listDom = taskList.map((item, i) => {

      const classes = ['taskText']
      if (item.done) classes.push('taskDone')

      return <li key={i}>
        <span className={classes.join(" ")} onClick={toggleTask(item.id)} >{item.task}</span>
        <span onClick={removeTask(item.id)} className="removeBtn">
          <SvgComponent component="trash" size="20" color="black" hoverColor="red" />
        </span>
      </li >;
    })
    return listDom
  }

  return (
    <section className="mainSection">
      <h1>ToDo</h1>
      <input value={taskInputValue} onChange={onTaskInput}></input>
      <button onClick={(submitTask)}>Submit Task</button>
      <ul>
        <ListOfTasks />
      </ul>
    </section>
  )
}

export default App
